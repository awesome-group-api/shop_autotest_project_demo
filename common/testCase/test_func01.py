import pytest

#
# # 定义函数类型01
# def test_tc01():
#     assert 1+1 == 2
# def test_tc02():
#     assert  1+1 == 3
#封装类02
class TestLogin:#登录接口
    #该测试类--前置条件--初始化（比如在购物前需要先登录，可把登录放在这里）
    def setup_class(self):
        print('执行测试类之前，我需要执行操作----')
    @pytest.mark.parametrize('a,b',[(1,2),(3,4),(5,6)])#('变量名',[1,2,3])
    def test_login01(self,a,b):
        print('---test_login01----')
        assert a + 1 == b  #断言

    def test_tc02(self):
        print('---test_login02----')
        assert  1+1 == 3 #断言

    def teardown_class(self):
        print("-----该测试类环境清除------")

if __name__== '__main__':
    pytest.main(['test_func01.py','-s'])